#!/bin/sh

DEST_FOLDER=/var/www/u0114284/apps/lunch-api/
CURRENT_FOLDER=$DEST_FOLDER"current"
RELEASE=$1"-$(date +%s)"
DEPLOY_FOLDER=$DEST_FOLDER$RELEASE

mkdir -p $DEPLOY_FOLDER
mkdir $DEPLOY_FOLDER/logs
mkdir $DEPLOY_FOLDER/logs/requests

unzip /var/www/u0114284/tmp/deploy.zip -d $DEPLOY_FOLDER
rm /var/www/u0114284/tmp/deploy.zip

mv /var/www/u0114284/tmp/.env $DEPLOY_FOLDER

echo "RELEASE="$1 >> "$DEPLOY_FOLDER/.env"

ln -sfn $DEPLOY_FOLDER $CURRENT_FOLDER
