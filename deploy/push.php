<?php

$deployDir = __DIR__ . DIRECTORY_SEPARATOR;

$cwd = __DIR__ . DIRECTORY_SEPARATOR . '..';

$gitStatus = command_exec('git status --porcelain', $cwd);

if (strlen($gitStatus) > 0) {
  exit("\e[31m\e[7mWorking folder is not clean!\e[27m\e[0m\n");
}

$zipFile = sys_get_temp_dir() . "/deploy.zip";

if (file_exists($zipFile)) {
  unlink($zipFile);
}

$folders = ['app', 'public', 'src', 'vendor'];
zip_files($cwd, $folders, $zipFile);
command_exec('scp ' . $zipFile . ' u0114284@scp106.hosting.reg.ru:/var/www/u0114284/tmp/', $cwd);
$envFile = __DIR__ . "/.env";
command_exec('scp ' . $envFile . ' u0114284@scp106.hosting.reg.ru:/var/www/u0114284/tmp/', $cwd);

$scriptFile = __DIR__ . "/deploy.sh";
$commitId = command_exec('git show -s --format=%h', $cwd);
$commitId = preg_replace('/\s*/', '', $commitId);

command_exec('ssh u0114284@scp106.hosting.reg.ru "bash -s" < ' . $scriptFile . ' ' . $commitId, $cwd);

$logFile = $deployDir . 'deploy.log';
$event = date("Y-m-d H:i:s") . " $commitId\n";
file_put_contents($logFile, $event, FILE_APPEND | LOCK_EX);

function command_exec($command, $cwd)
{
  $descriptorspec = array(
    0 => array("pipe", "r"),  // stdin - канал, из которого дочерний процесс будет читать
    1 => array("pipe", "w"),  // stdout - канал, в который дочерний процесс будет записывать
    2 => array("pipe", "w")   // stderr - файл для записи
  );

  $process = proc_open($command, $descriptorspec, $pipes, $cwd);

  if (is_resource($process)) {
    // $pipes теперь выглядит так:
    // 0 => записывающий обработчик, подключённый к дочернему stdin
    // 1 => читающий обработчик, подключённый к дочернему stdout
    // 2 => читающий обработчик, подключённый к дочернему stderr

    fclose($pipes[0]);

    $output = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $errors = stream_get_contents($pipes[2]);
    fclose($pipes[2]);

    $errors = str_replace("stdin: is not a tty\n", '', $errors);
    if (strlen($errors) > 0) {
      print($errors);
      print("\n");
    }

    // Важно закрывать все каналы перед вызовом
    // proc_close во избежание мёртвой блокировки
    $return_value = proc_close($process);
  }

  return $output;
}

function zip_files($parent, $folders, $destination)
{
  $zip = new ZipArchive();
  if ($zip->open($destination, ZIPARCHIVE::CREATE) === true) {
    $parentFolder = realpath($parent);
    foreach ($folders as $folder) {
      $source = realpath($parentFolder . DIRECTORY_SEPARATOR . $folder);
      if (is_dir($source)) {
        $iterator = new RecursiveDirectoryIterator($source);
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
        foreach ($files as $file) {
          $file = realpath($file);
          $zipPath = str_replace($parentFolder . DIRECTORY_SEPARATOR, '', $file);
          $zipPath = str_replace('\\', '/', $zipPath);
          if (is_dir($file) && (count(glob("$file/*")) === 0)) {
            $zip->addEmptyDir($zipPath);
          } elseif (is_file($file)) {
            $zip->addFile($file, $zipPath);
          }
        }
      }
    }
  } else {
    exit('Error open ZIP-file.');
  }
  return $zip->close();
}
