<?php
declare(strict_types=1);

namespace App\Domain\Dish;

use App\Domain\DomainException\DomainRecordExistsException;

class DishAlreadyExistsException extends DomainRecordExistsException
{
    public $message = 'The dish you try add already exist.';
}
