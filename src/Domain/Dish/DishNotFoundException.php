<?php
declare(strict_types=1);

namespace App\Domain\Dish;

use App\Domain\DomainException\DomainRecordNotFoundException;

class DishNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The dish you requested does not exist.';
}
