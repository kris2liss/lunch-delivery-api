<?php
declare(strict_types=1);

namespace App\Domain\Dish;

use JsonSerializable;

class Dish implements JsonSerializable
{
    private ?int $id;

    private string $name;

    private int $typeId;

    private string $comment;


    public function __construct(?int $id, string $name, int $typeId, string $comment)
    {
        $this->id = $id;
        $this->name = strtolower($name);
        $this->typeId = $typeId;
        $this->comment = ($comment);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTypeId(): int
    {
        return $this->typeId;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'typeId' => $this->typeId,
            'comment' => $this->comment,
        ];
    }
}
