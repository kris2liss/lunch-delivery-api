<?php
declare(strict_types=1);

namespace App\Domain\Dish;

interface DishRepository
{
    public function addDish(string $name, string $typeId, string $comment);

    public function findAll(): array;

    public function findDishOfId(int $id): Dish;

    public function updateDishOfId(int $id, array $fields);

    public function deleteDishOfId(int $id);
}
