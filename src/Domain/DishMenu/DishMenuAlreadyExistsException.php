<?php
declare(strict_types=1);

namespace App\Domain\DishMenu;

use App\Domain\DomainException\DomainRecordExistsException;

class DishMenuAlreadyExistsException extends DomainRecordExistsException
{
    public $message = 'The dish you try add to menu already added.';
}
