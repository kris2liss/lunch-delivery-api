<?php
declare(strict_types=1);

namespace App\Domain\DishMenu;

interface DishMenuRepository
{    
    public function findMenuOfDate(string $date): array;

    public function findLastMenu(string $dateFrom, string $dateTo): array;

    public function findAllDates(): array;

    public function addDishToMenu(int $dishId, string $menuDate);

    public function removeDishFromMenu(int $dishId, string $menuDate);

}
