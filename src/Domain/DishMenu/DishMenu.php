<?php
declare(strict_types=1);

namespace App\Domain\DishMenu;

use JsonSerializable;

class DishMenu implements JsonSerializable
{
    private ?int $id;

    private int $dishId;

    private string $menuDate;

    public function __construct(?int $id, int $dishId, string $menuDate)
    {
        $this->id = $id;
        $this->dishId = $dishId;
        $this->menuDate = $menuDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDishId(): int
    {
        return $this->dishId;
    }

    public function getMenuDate(): string
    {
        return $this->menuDate;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'dishId' => $this->dishId,
            'menuDate' => $this->menuDate
        ];
    }
}
