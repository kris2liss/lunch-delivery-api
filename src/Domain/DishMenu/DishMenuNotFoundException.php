<?php
declare(strict_types=1);

namespace App\Domain\DishMenu;

use App\Domain\DomainException\DomainRecordNotFoundException;

class DishMenuNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The dish in menu you requested does not exist.';
}
