<?php
declare(strict_types=1);

namespace App\Domain\DishType;

use App\Domain\DomainException\DomainRecordExistsException;

class DishTypeAlreadyExistsException extends DomainRecordExistsException
{
    public $message = 'The dish type you try add already exist.';
}
