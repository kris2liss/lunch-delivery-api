<?php
declare(strict_types=1);

namespace App\Domain\DishType;

use JsonSerializable;

class DishType implements JsonSerializable
{
    private ?int $id;

    private string $name;
    private string $type;
    private int $order;
    private int $price;


    public function __construct(?int $id, string $name, string $type, int $order, int $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->order = $order;
        $this->price = $price;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'order' => $this->order,
            'price' => $this->price,
        ];
    }
}
