<?php
declare(strict_types=1);

namespace App\Domain\DishType;

interface DishTypeRepository
{
    public function addDishType(string $name, string $type, int $order, int $price);

    /**
     * @return DishType[]
     */
    public function findAll(): array;

    /**
     * @throws DishNotFoundException
     */
    public function findDishTypeOfId(int $id): DishType;

    public function deleteDishTypeOfId(int $id);

    /**
     * @throws DishNotFoundException
     */
    public function updateDishTypeOfId(int $id, string $name, string $type, int $order, int $price);
}
