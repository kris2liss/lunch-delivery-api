<?php
declare(strict_types=1);

namespace App\Domain\DishType;

use App\Domain\DomainException\DomainRecordNotFoundException;

class DishTypeNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'The dish type you requested does not exist.';
}
