<?php

declare(strict_types=1);

namespace App\Application\Database;

use App\Application\Exception\DatabaseException;
use Medoo\Medoo;
use PDOException;

class Database implements DatabaseInterface
{
    /**
     * @var array
     */
    private Medoo $database;

    /**
     * Settings constructor.
     * @param array $settings
     */
    public function __construct(string $databaseFile)
    {
        if (file_exists($databaseFile)) {
            $this->setDatabase($databaseFile);
        } else {
            $this->setDatabase($databaseFile);
            DefaultData::init($this->database);
        }
    }

    private function setDatabase(string $databaseFile) {
        $this->database = new Medoo([
            'type' => 'sqlite',
            'database' => $databaseFile,
            'command' => [
                'PRAGMA foreign_keys = ON'
            ]
        ]);
    }

    public function getById(string $table, int $id)
    {
        try {
            $result = $this->database->select($table, '*', ['id' => $id]);
            return $result;
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }

    public function get(string $table, ?array $joinTable = null, ?array $columns = null, ?array $where = null)
    {
        if ($columns == null) {
            $columns = '*';
        }

        try {
            if ($joinTable) {
                $result = $this->database->select($table, $joinTable, $columns, $where);
            } else {
                $result = $this->database->select($table, $columns, $where);
            }
            return $result;
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }

    public function getAll(string $table)
    {
        try {
            return $this->get($table);
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }

    public function getWhere(string $table, array $where)
    {
        try {
            return $this->get($table, null, null, $where);
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }

    public function deleteByFields(string $table, array $fields)
    {
        try {
            $this->database->delete($table, $fields);
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }

    public function deleteById(string $table, int $id)
    {
        try {
            $this->database->delete($table, ['id' => $id]);
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }

    public function updateById(string $table, int $id, array $fields)
    {
        try {
            $this->database->update($table, $fields, ['id' => $id]);
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }

    public function add(string $table, array $col)
    {
        try {
            $this->database->insert($table, $col);
        } catch (PDOException $e) {
            throw new DatabaseException($e);
        }
    }
}
