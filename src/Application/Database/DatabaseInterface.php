<?php
declare(strict_types=1);

namespace App\Application\Database;

interface DatabaseInterface
{
    public function add(string $table, array $col);

    public function get(string $table, ?array $joinTable = null, ?array $columns = null, ?array $where = null);

    public function getAll(string $table);

    public function getWhere(string $table, array $where);

    public function getById(string $table, int $id);

    public function deleteByFields(string $table, array $fields);

    public function updateById(string $table, int $id, array $fields);

    public function deleteById(string $table, int $id);
}
