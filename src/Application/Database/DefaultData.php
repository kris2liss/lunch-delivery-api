<?php

declare(strict_types=1);

namespace App\Application\Database;

use App\Application\Exception\DatabaseException;
use Medoo\Medoo;
use PDOException;

class DefaultData
{
  public static function init(Medoo $database)
  {
    DefaultData::createTable($database);
    // DefaultData::insertDefaultData($database);
  }

  private static function createTable(Medoo $database)
  {
    $database->create("dish_types", [
      "id" => [
        "INTEGER",
        "PRIMARY KEY"
      ],
      "name" => [
        "VARCHAR(255)",
        "NOT NULL",
        "UNIQUE",
        "CHECK (length(name) > 0)"
      ],
      "type" => [
        "VARCHAR(255)",
        "NOT NULL",
        "UNIQUE",
        "CHECK (length(type) > 0)"
      ],
      "order" => [
        "INTEGER",
        "NOT NULL",
        "UNIQUE"
      ],
      "price" => [
        "INTEGER",
        "NOT NULL"
      ]
    ]);

    $database->create("dishes", [
      "id" => [
        "INTEGER",
        "PRIMARY KEY"
      ],
      "name" => [
        "VARCHAR(255)",
        "NOT NULL",
        "UNIQUE",
        "CHECK (length(name) > 0)"
      ],
      "typeId" => [
        "INT",
        "NOT NULL"
      ],
      "comment" => [
        "VARCHAR(255)",
        "NOT NULL"
      ],
      "FOREIGN KEY (typeId) 
              REFERENCES dish_types (id) 
                  ON DELETE NO ACTION 
                  ON UPDATE NO ACTION"
    ]);

    $database->create("dishes_menus", [
      "id" => [
        "INTEGER",
        "PRIMARY KEY"
      ],
      "dishId" => [
        "INT",
        "NOT NULL"
      ],
      "menuDate" => [
        "VARCHAR(255)",
        "NOT NULL",
        "CHECK (date(menuDate) IS NOT NULL)"
      ],
      "UNIQUE(dishId,menuDate)",
      "FOREIGN KEY (dishId) 
              REFERENCES dishes (id) 
                  ON DELETE NO ACTION 
                  ON UPDATE NO ACTION"
    ]);
  }

  private static function insertDefaultData(Medoo $database)
  {
    $defaultDataText = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'DefaultData.json');

    $defaulData = json_decode($defaultDataText, true);
    foreach ($defaulData as $tableName => $fields) {
      try {
        $database->insert($tableName, $fields);
      } catch (PDOException $e) {
        $databaseException = new DatabaseException($e);
        if ($databaseException->getCode() != "23000") {
          $databaseException->addMessage("Error when try insert default data to database.");
          throw $databaseException;
        }
      }
    }
  }
}
