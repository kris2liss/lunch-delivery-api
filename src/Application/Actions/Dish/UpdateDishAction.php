<?php

declare(strict_types=1);

namespace App\Application\Actions\Dish;

use Psr\Http\Message\ResponseInterface as Response;

class updateDishAction extends DishAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dishId = (int) $this->resolveArg('id');

        $parsedBody = $this->request->getParsedBody();

        $this->dishRepository->updateDishOfId($dishId, $parsedBody);

        return $this->respondWithData(['status' => 'success']);
    }
}
