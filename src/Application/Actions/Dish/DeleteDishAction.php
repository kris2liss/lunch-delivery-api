<?php

declare(strict_types=1);

namespace App\Application\Actions\Dish;

use Psr\Http\Message\ResponseInterface as Response;

class DeleteDishAction extends DishAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dishId = (int) $this->resolveArg('id');
        $this->dishRepository->deleteDishOfId($dishId);

        return $this->respondWithData(['status' => 'success']);
    }
}
