<?php

declare(strict_types=1);

namespace App\Application\Actions\Dish;

use Psr\Http\Message\ResponseInterface as Response;

class CreateDishAction extends DishAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $parsedBody = $this->request->getParsedBody();

        $this->dishRepository->addDish($parsedBody["name"], $parsedBody["typeId"], $parsedBody["comment"]);

        return $this->respondWithData(['status' => 'success']);
    }
}
