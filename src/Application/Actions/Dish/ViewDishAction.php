<?php

declare(strict_types=1);

namespace App\Application\Actions\Dish;

use Psr\Http\Message\ResponseInterface as Response;

class ViewDishAction extends DishAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dishId = (int) $this->resolveArg('id');
        $dish = $this->dishRepository->findDishOfId($dishId);

        return $this->respondWithData($dish);
    }
}
