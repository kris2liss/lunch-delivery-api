<?php

declare(strict_types=1);

namespace App\Application\Actions\Dish;

use App\Application\Actions\Action;
use App\Domain\Dish\DishRepository;

abstract class DishAction extends Action
{
    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * @param DishRepository $menuRepository
     */
    public function __construct(DishRepository $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }
}
