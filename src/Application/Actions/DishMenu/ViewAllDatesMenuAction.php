<?php

declare(strict_types=1);

namespace App\Application\Actions\DishMenu;

use Psr\Http\Message\ResponseInterface as Response;

class ViewAllDatesMenuAction extends DishMenuAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $menu = $this->dishMenuRepository->findAllDates();

        return $this->respondWithData($menu);
    }
}
