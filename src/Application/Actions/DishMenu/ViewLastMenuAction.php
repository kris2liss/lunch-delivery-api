<?php

declare(strict_types=1);

namespace App\Application\Actions\DishMenu;

use Psr\Http\Message\ResponseInterface as Response;

class ViewLastMenuAction extends DishMenuAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dateFrom = $this->resolveArg('dateFrom');
        $dateTo = $this->resolveArg('dateTo');
        $menu = $this->dishMenuRepository->findLastMenu($dateFrom, $dateTo);

        return $this->respondWithData($menu);
    }
}
