<?php

declare(strict_types=1);

namespace App\Application\Actions\DishMenu;

use Psr\Http\Message\ResponseInterface as Response;

class DeleteDishFromMenuAction extends DishMenuAction
{
    protected function action(): Response
    {
        $dishId = (int) $this->resolveArg('dishId');

        $menuDate = (string) $this->resolveArg('menuDate');

        $this->dishMenuRepository->removeDishFromMenu($dishId, $menuDate);

        return $this->respondWithData(['status' => 'success']);
    }
}
