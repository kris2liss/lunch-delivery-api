<?php

declare(strict_types=1);

namespace App\Application\Actions\DishMenu;

use App\Application\Actions\Action;
use App\Domain\DishMenu\DishMenuRepository;

abstract class DishMenuAction extends Action
{
    protected $dishMenuRepository;

    public function __construct(DishMenuRepository $dishMenuRepository)
    {
        $this->dishMenuRepository = $dishMenuRepository;
    }
}
