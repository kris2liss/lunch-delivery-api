<?php

declare(strict_types=1);

namespace App\Application\Actions\DishMenu;

use Psr\Http\Message\ResponseInterface as Response;

class AddDishToMenuAction extends DishMenuAction
{
    protected function action(): Response
    {
        $dishId = (int) $this->resolveArg('dishId');

        $menuDate = $this->resolveArg('menuDate');

        $this->dishMenuRepository->addDishToMenu($dishId, $menuDate);

        return $this->respondWithData(['status' => 'success']);
    }
}
