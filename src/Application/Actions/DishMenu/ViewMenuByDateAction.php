<?php

declare(strict_types=1);

namespace App\Application\Actions\DishMenu;

use Psr\Http\Message\ResponseInterface as Response;

class ViewMenuByDateAction extends DishMenuAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $menuDate = $this->resolveArg('date');
        $menu = $this->dishMenuRepository->findMenuOfDate($menuDate);

        return $this->respondWithData($menu);
    }
}
