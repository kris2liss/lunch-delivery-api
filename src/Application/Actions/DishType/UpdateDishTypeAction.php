<?php

declare(strict_types=1);

namespace App\Application\Actions\DishType;

use Psr\Http\Message\ResponseInterface as Response;

class updateDishTypeAction extends DishTypeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dishTypeId = (int) $this->resolveArg('id');

        $parsedBody = $this->request->getParsedBody();

        $this->dishTypeRepository->updateDishTypeOfId($dishTypeId, $parsedBody["name"], $parsedBody["type"], (int) $parsedBody["order"], (int) $parsedBody["price"]);

        return $this->respondWithData(['status' => 'success']);
    }
}
