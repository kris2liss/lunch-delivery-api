<?php

declare(strict_types=1);

namespace App\Application\Actions\DishType;

use App\Application\Actions\Action;
use App\Domain\DishType\DishTypeRepository;

abstract class DishTypeAction extends Action
{
    protected $dishTypeRepository;

    public function __construct(DishTypeRepository $dishTypeRepository)
    {
        $this->dishTypeRepository = $dishTypeRepository;
    }
}
