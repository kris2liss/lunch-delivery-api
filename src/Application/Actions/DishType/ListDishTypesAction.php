<?php

declare(strict_types=1);

namespace App\Application\Actions\DishType;

use Psr\Http\Message\ResponseInterface as Response;

class ListDishTypesAction extends DishTypeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dishTypes = $this->dishTypeRepository->findAll();

        return $this->respondWithData($dishTypes);
    }
}
