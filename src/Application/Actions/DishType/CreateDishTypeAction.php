<?php

declare(strict_types=1);

namespace App\Application\Actions\DishType;

use Psr\Http\Message\ResponseInterface as Response;

class CreateDishTypeAction extends DishTypeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $parsedBody = $this->request->getParsedBody();

        $this->dishTypeRepository->addDishType($parsedBody["name"], $parsedBody["type"], (int) $parsedBody["order"], (int) $parsedBody["price"]);

        return $this->respondWithData(['status' => 'success']);
    }
}
