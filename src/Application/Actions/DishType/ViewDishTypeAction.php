<?php

declare(strict_types=1);

namespace App\Application\Actions\DishType;

use Psr\Http\Message\ResponseInterface as Response;

class ViewDishTypeAction extends DishTypeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $dishTypeId = (int) $this->resolveArg('id');
        $dishType = $this->dishTypeRepository->findDishTypeOfId($dishTypeId);

        return $this->respondWithData($dishType);
    }
}
