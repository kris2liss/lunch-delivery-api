<?php

declare(strict_types=1);

namespace App\Application\Exception;

use PDOException;

class DatabaseException extends PDOException
{
  public function __construct(PDOException $e)
  {
    if (strstr($e->getMessage(), 'SQLSTATE[')) {
      preg_match('/SQLSTATE\[(\w+)\]: (.+)/', $e->getMessage(), $matches);
      $this->code = $matches[1];
      $this->message = $matches[2];
    }
  }

  public function addMessage(string $message) {
    $this->message = $message . ' ' . $this->message;
  }
}
