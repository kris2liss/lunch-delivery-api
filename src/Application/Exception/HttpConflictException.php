<?php

declare(strict_types=1);

namespace App\Application\Exception;

use Slim\Exception\HttpSpecializedException;

class HttpConflictException extends HttpSpecializedException
{
    /**
     * @var int
     */
    protected $code = 409;

    /**
     * @var string
     */
    protected $message = 'Conflict.';

    protected $title = '404 Conflict';
    protected $description = 'The request could not be completed due to a conflict with the current state of the target resource';
}
