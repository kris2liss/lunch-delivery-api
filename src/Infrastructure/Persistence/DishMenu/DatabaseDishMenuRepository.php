<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\DishMenu;

use App\Domain\DishMenu\DishMenuAlreadyExistsException;
use App\Domain\DishMenu\DishMenuRepository;
use App\Domain\DishMenu\DishMenuNotFoundException;
use App\Application\Database\DatabaseInterface;

class DatabaseDishMenuRepository implements DishMenuRepository
{
    private DatabaseInterface $database;
    private const tableName = 'dishes_menus';

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function addDishToMenu(int $dishId, string $menuDate)
    {
        if ($this->isExistsDishMenu($dishId, $menuDate)) {
            throw new DishMenuAlreadyExistsException();
        }
        $this->database->add(self::tableName, ['dishId' => $dishId, 'menuDate' => $menuDate]);
    }

    public function removeDishFromMenu(int $dishId, string $menuDate)
    {
        if (!$this->isExistsDishMenu($dishId, $menuDate)) {
            throw new DishMenuNotFoundException();
        }
        $this->database->deleteByFields(self::tableName, ['dishId' => $dishId, 'menuDate' => $menuDate]);
    }

    private function isExistsDishMenu(int $dishId, string $menuDate): bool
    {
        return !!$this->database->getWhere(self::tableName, ['dishId' => $dishId, 'menuDate' => $menuDate]);
    }


    public function findMenuOfDate(string $date): array
    {
        $menuDishes = $this->database->get(
            self::tableName,
            [
                '[>]dishes' => ['dishes_menus.dishId' => 'id'],
                '[>]dish_types' => ['dishes.typeId' => 'id'],
            ],
            [
                'dishes.id (id)',
                'dishes.name (name)',
                'dishes.comment (comment)',
                'dishes.typeId (typeId)',
                'dish_types.name (typeName)',
                'dish_types.type (type)',
                'dish_types.order (typeOrder)',
            ],
            ['menuDate' => $date]
        );

        return $menuDishes;
    }

    public function findLastMenu(string $dateFrom, string $dateTo): array
    {
        $menuDishes = $this->database->get(
            'dishes_menus',
            [
                '[>]dishes' => ['dishes_menus.dishId' => 'id'],
                '[>]dish_types' => ['dishes.typeId' => 'id'],
            ],
            [
                'menuDate',
                'dishes.id (id)',
                'dishes.name (name)',
                'dishes.comment (comment)',
                'dishes.typeId (typeId)',
                'dish_types.name (typeName)',
                'dish_types.type (type)',
                'dish_types.order (typeOrder)',
            ],
            [
                'menudate[<>]' => [$dateFrom, $dateTo]
            ]
        );
        $dates = array_unique(array_map(
            function ($el) {
                return $el['menuDate'];
            },
            $menuDishes
        ));

        $menus = [];
        foreach ($dates as $date) {
            $menus[$date] = [];
            foreach ($menuDishes as $dish) {
                $dish['menuDate'] === $date && $menus[$date][] = $dish;
            }
        }

        return $menus;
    }

    public function findAllDates(): array
    {
        $menuDates = $this->database->get(
            'dishes_menus',
            [],
            ['@dishes_menus.menuDate'],
            ['ORDER' => ['menuDate'  => 'DESC']]
        );

        return $menuDates;
    }
}
