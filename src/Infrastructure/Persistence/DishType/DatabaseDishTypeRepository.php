<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\DishType;

use App\Application\Exception\DatabaseException;
use App\Domain\DishType\DishType;
use App\Domain\DishType\DishTypeNotFoundException;
use App\Domain\DishType\DishTypeAlreadyExistsException;
use App\Domain\DishType\DishTypeRepository;
use App\Application\Database\DatabaseInterface;

class DatabaseDishTypeRepository implements DishTypeRepository
{
    private DatabaseInterface $database;
    private const tableName = "dish_types";

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function addDishType(string $name, string $type, int $order, int $price)
    {
        try {
            $this->database->add(self::tableName, ['name' => $name, 'type' => $type, 'order' => $order, 'price' => $price]);
        } catch (DatabaseException $exception) {
            if ($exception->getCode() == "23000") {
                throw new DishTypeAlreadyExistsException();
            }
        }
    }

    public function findAll(): array
    {
        return array_values($this->database->getAll(self::tableName));
    }

    public function findDishTypeOfId(int $id): DishType
    {
        $result = $this->database->getById(self::tableName, $id);
        if (count($result) == 0) {
            throw new DishTypeNotFoundException();
        }
        $dishType = new DishType((int)$result[0]["id"], $result[0]["name"], $result[0]["type"], $result[0]["order"], $result[0]["price"]);
        return $dishType;
    }

    public function deleteDishTypeOfId(int $id)
    {
        $this->isExistsDishTypeOfId($id);
        $this->database->deleteById(self::tableName, $id);
    }

    public function updateDishTypeOfId(int $id, string $name, string $type, int $order, int $price)
    {
        $this->isExistsDishTypeOfId($id);
        $this->database->updateById(self::tableName, $id, ['name' => $name, 'type' => $type, 'order' => $order, 'price' => $price]);
    }

    private function isExistsDishTypeOfId(int $id) {
        $result = $this->database->getById(self::tableName, $id);
        if (count($result) == 0) {
            throw new DishTypeNotFoundException();
        }
    }
}
