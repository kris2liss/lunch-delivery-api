<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Dish;

use App\Application\Exception\DatabaseException;
use App\Domain\Dish\Dish;
use App\Domain\Dish\DishNotFoundException;
use App\Domain\Dish\DishAlreadyExistsException;
use App\Domain\Dish\DishRepository;
use App\Application\Database\DatabaseInterface;

class DatabaseDishRepository implements DishRepository
{
    private DatabaseInterface $database;
    private const tableName = "dishes";

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function addDish(string $name, string $typeId, string $comment)
    {
        $this->database->add(self::tableName, ['name' => $name, 'typeId' => $typeId, 'comment' => $comment]);
    }

    public function findAll(): array
    {
        return array_values($this->database->get(
            self::tableName, 
            ["[><]dish_types" => ["typeId" => "id"]], 
            [
                self::tableName . ".id (id)", 
                self::tableName . ".name (name)",
                self::tableName . ".comment (comment)",
                self::tableName . ".typeId (typeId)",
                "dish_types.name (typeName)",
                "dish_types.type (type)",
                'dish_types.order (typeOrder)',
            ]
        ));
    }

    public function findDishOfId(int $id): Dish
    {
        $result = $this->database->getById(self::tableName, $id);
        if (count($result) == 0) {
            throw new DishNotFoundException();
        }
        $dishType = new Dish((int)$result[0]["id"], $result[0]["name"], $result[0]["typeId"], $result[0]["comment"]);
        return $dishType;
    }

    public function deleteDishOfId(int $id)
    {
        $this->isExistsDishOfId($id);
        $this->database->deleteById(self::tableName, $id);
    }

    public function updateDishOfId(int $id, array $fields)
    {
        $this->isExistsDishOfId($id);
        $this->database->updateById(self::tableName, $id, $fields);
    }

    private function isExistsDishOfId(int $id) {
        $result = $this->database->getById(self::tableName, $id);
        if (count($result) == 0) {
            throw new DishNotFoundException();
        }
    }
}
