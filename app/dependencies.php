<?php

declare(strict_types=1);

use App\Application\Database\Database;
use App\Application\Database\DatabaseInterface;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        DatabaseInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);
            return new Database($settings->get('databaseFile'));
        },
    ]);
};
