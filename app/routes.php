<?php

declare(strict_types=1);

use App\Application\Actions\Dish\ListDishesAction;
use App\Application\Actions\Dish\ViewDishAction;
use App\Application\Actions\Dish\CreateDishAction;
use App\Application\Actions\Dish\DeleteDishAction;
use App\Application\Actions\Dish\UpdateDishAction;

use App\Application\Actions\DishType\ListDishTypesAction;
use App\Application\Actions\DishType\ViewDishTypeAction;
use App\Application\Actions\DishType\CreateDishTypeAction;
use App\Application\Actions\DishType\DeleteDishTypeAction;
use App\Application\Actions\DishType\UpdateDishTypeAction;

use App\Application\Actions\DishMenu\AddDishToMenuAction;
use App\Application\Actions\DishMenu\DeleteDishFromMenuAction;
use App\Application\Actions\DishMenu\ViewLastMenuAction;
use App\Application\Actions\DishMenu\ViewAllDatesMenuAction;
use App\Application\Actions\DishMenu\ViewMenuByDateAction;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

use Tuupola\Middleware\HttpBasicAuthentication;

return function (App $app) {
    $app->group(
        '/api',
        function (Group $app) {
            $readAuth = new HttpBasicAuthentication([
                "users" => [
                    "api_key_read" => $_ENV['API_KEY_READ'],
                    "api_key_write" => $_ENV['API_KEY_WRITE']
                ]
            ]);
            $writeAuth = new HttpBasicAuthentication([
                "users" => [
                    "api_key_write" => $_ENV['API_KEY_WRITE']
                ]
            ]);

            $app->options('/{routes:.*}', function (Request $request, Response $response) {
                // CORS Pre-Flight OPTIONS Request Handler
                return $response->withStatus(204, 'No Content');
            });
            
            $app->group('/menus', function (Group $group) {
                $group->get('/all-dates', ViewAllDatesMenuAction::class);
                $group->get('/{dateFrom}/{dateTo}', ViewLastMenuAction::class);
                $group->get('/{date}', ViewMenuByDateAction::class);
            })->add($readAuth);

            $app->group('/dishes', function (Group $group) {
                $group->get('', ListDishesAction::class);
                $group->get('/{id:[0-9]+}', ViewDishAction::class);
            })->add($readAuth);

            $app->group('/dish-types', function (Group $group) {
                $group->get('', ListDishTypesAction::class);
                $group->get('/{id:[0-9]+}', ViewDishTypeAction::class);
            })->add($readAuth);

            $app->group('/menus/{menuDate}/dishes/{dishId:[0-9]+}', function (Group $group) {
                $group->post('', AddDishToMenuAction::class);
                $group->delete('', DeleteDishFromMenuAction::class);
            })->add($writeAuth);

            $app->group('/dishes', function (Group $group) {
                $group->post('', CreateDishAction::class);
                $group->delete('/{id:[0-9]+}', DeleteDishAction::class);
                $group->patch('/{id:[0-9]+}', UpdateDishAction::class);
            })->add($writeAuth);

            $app->group('/dish-types', function (Group $group) {
                $group->post('', CreateDishTypeAction::class);
                $group->delete('/{id:[0-9]+}', DeleteDishTypeAction::class);
                $group->patch('/{id:[0-9]+}', UpdateDishTypeAction::class);
            })->add($writeAuth);
        }
    );
};
