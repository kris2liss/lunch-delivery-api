<?php

declare(strict_types=1);

$event = [
  'requestId' => REQUEST_ID,
  'release' => $_ENV['RELEASE']
];

$event['request'] = [
  'method' => $_SERVER['REQUEST_METHOD'],
  'query' => $_GET,
  'post' => json_decode(file_get_contents('php://input'), true),
  'uri' => $_SERVER['REQUEST_URI'],
  'headers' => getRequestHeaders()
];

$event['response'] = [
  'code' => $response->getStatusCode(),
  'headers' => headers_list(),
  'body' => defined('RESPONSE_BODY_LOG') ? json_decode($response->getBody()->__toString()) : ''
];

$event['metrics'] = [];

if (!stristr(PHP_OS, "win")) {
  $avgLoad = sys_getloadavg();
  $event['metrics']['server'] = [];
  $event['metrics']['server']['avg_load'] = [
    '1m' => $avgLoad[0],
    '5m' => $avgLoad[1],
    '15m' => $avgLoad[2],
  ];
}

$event['metrics']['php'] = [
  'memory' => memory_get_usage(),
  'pick' => memory_get_peak_usage()
];

$event['metrics']['transaction'] = [
  'start' => REQUEST_START,
  'begin' => APP_START,
  'autoload' => APP_AUTOLOAD,
  'bootstrap' => APP_BOOTSTRAP,
  'handle' => APP_HANDLE,
  'emit' => APP_EMIT
];

$event['client'] = [
  'ip' => $_SERVER['REMOTE_ADDR'],
];

$microtimeString = explode(' ', microtime());
$timestamp = $microtimeString[1] . substr($microtimeString[0], 1, 7);

$event['timestamp'] = $timestamp;

$timeZone = new DateTimeZone('Europe/Moscow');

$time = new DateTime();
$time->setTimestamp((int) $microtimeString[1])->setTimezone($timeZone);

$formattedDate = $time->format('Y-m-d');
$formattedTime = $time->format('H-i-s');

$logDir = __DIR__ . '/../logs/requests/' . $formattedDate . '/';
$logFile = $logDir . $formattedTime . '.' . REQUEST_ID . '.json';

if (!is_dir($logDir)) {
  mkdir($logDir);
}

writeJson($logFile, $event);

function writeJson($filename, $event)
{
  // write only to new file, else skip writting
  if (!file_exists($filename)) {
    $handle = fopen($filename, 'w+');
    if ($handle) {
      fwrite($handle, json_encode($event, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
      fclose($handle);
    }
  }
}

function getRequestHeaders()
{
  $headers = array();
  foreach ($_SERVER as $key => $value) {
    if (strpos($key, 'HTTP_') === 0) {
      $headers[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value;
    }
  }
  return $headers;
}
