<?php
declare(strict_types=1);

use App\Domain\Dish\DishRepository;
use App\Domain\DishMenu\DishMenuRepository;
use App\Domain\DishType\DishTypeRepository;
use App\Infrastructure\Persistence\Dish\DatabaseDishRepository;
use App\Infrastructure\Persistence\DishMenu\DatabaseDishMenuRepository;
use App\Infrastructure\Persistence\DishType\DatabaseDishTypeRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        DishRepository::class => \DI\autowire(DatabaseDishRepository::class),
        DishMenuRepository::class => \DI\autowire(DatabaseDishMenuRepository::class),
        DishTypeRepository::class => \DI\autowire(DatabaseDishTypeRepository::class),
    ]);
};
