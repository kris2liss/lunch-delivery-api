<?php

declare(strict_types=1);

define('REQUEST_START', $_SERVER['REQUEST_TIME_FLOAT']);

use App\Application\ResponseEmitter\ResponseEmitter;

define('APP_START', microtime(true));

define('REQUEST_ID', uniqIdReal());

header('X-Request-Id: ' . REQUEST_ID);
ignore_user_abort(true);
ob_start();

require __DIR__ . '/../vendor/autoload.php';

define('APP_AUTOLOAD', microtime(true));

require __DIR__ . '/../app/bootstrap.php';

define('APP_BOOTSTRAP', microtime(true));

// Run App & Emit Response
$response = $app->handle($request);

define('APP_HANDLE', microtime(true));

$responseEmitter = new ResponseEmitter();
$responseEmitter->emit($response);

$size = ob_get_length();
header("Content-Length: $size");

ob_end_flush();
flush();

define('APP_EMIT', microtime(true));

require __DIR__ . '/../app/logger.php';

function uniqIdReal($lenght = 13)
{
  if (function_exists("random_bytes")) {
    $bytes = random_bytes((int) ceil($lenght / 2));
  } elseif (function_exists("openssl_random_pseudo_bytes")) {
    $bytes = openssl_random_pseudo_bytes((int) ceil($lenght / 2));
  } else {
    throw new Exception("no cryptographically secure random function available");
  }
  return substr(bin2hex($bytes), 0, $lenght);
}
